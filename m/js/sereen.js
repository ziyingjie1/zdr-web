// 移动端字体 begin
(function(doc, win) {
    var docEl = doc.documentElement,
        resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
        recalc = function() {
            var clientWidth = docEl.clientWidth;
            if (!clientWidth) return;
            docEl.style.fontSize = 100 * (clientWidth / 750) + 'px';
        };
    if (!doc.addEventListener) return;
    win.addEventListener(resizeEvt, recalc, false);
    doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);
// 移动端字体 end
$(function(){
    // 筛选 begin
    var sereenLen = $(".list_sereenNav li").length;
    $(".list_sereenNav li").on("click",function(e){
        var i = $(this).index();
        if($(this).hasClass("snon")){
            $(this).removeClass("snon");
            $(".list_sereenMess .list_sereen_box").eq(i).stop(true,true).slideUp("fast");
        }else{
            $(".list_sereenNav li").removeClass("snon");
            $(this).addClass("snon");
            $(".list_sereenMess .list_sereen_box").stop(true,true).slideUp("fast");
            $(".list_sereenMess .list_sereen_box").eq(i).stop(true,true).slideDown("fast");
        }
    })
    $(".list_sereenUp").on("click",function(e){
        var i = $(".list_sereenMess .list_sereenUp").index(this);
        $(this).parents(".list_sereen_box").stop(true,true).slideUp("fast");
        $(".list_sereenNav li").eq(i).removeClass("snon");
    })
    $(".list_sereen_box li").on("click",function(e){
        var i = $(".list_sereenMess .list_sereenUp").index(this);
        $(this).parents(".list_sereen_box").find(".none").removeClass("snon");
        $(this).parents(".list_sereen_check").find("li").removeClass("snon");
        $(this).addClass("snon");
        sereenUp.call(this);
        slectDucy.call(this);
    })
    $(".none").on("click",function(e){
        $(this).parents(".list_sereen_box").find("li").removeClass("snon");
        $(this).addClass("snon");
        var induVal = $('.indu_click .list_sereen_check .snon').html(),
            cityVal = $('.city .list_sereen_check .snon').html();
            induVal = (induVal== undefined ? induVal = "" : induVal)
            cityVal = (cityVal== undefined ? cityVal = "" : cityVal)
        if($(this).parents(".list_sereen_box").hasClass("city")){
            $(".list_creen_result").html(induVal + "展会");
        }else{
            $(".list_creen_result").html(cityVal + "展会");
        }
        if(induVal == '' && cityVal == ''){
            $(".list_creen_result").html("");
        }
        sereenUp.call(this)
    })
    // 判断选中元素是否具有undefind begin
    function slectDucy(){
        if($(this).parents(".list_sereen_box").hasClass("city") || $(this).parents(".list_sereen_box").hasClass("indu_click")){
            var induVal = $('.indu_click .list_sereen_check .snon').html(),
                cityVal = $('.city .list_sereen_check .snon').html();
                induVal = (induVal== undefined ? induVal = "" : induVal)
                cityVal = (cityVal== undefined ? cityVal = "" : cityVal)
            $(".list_creen_result").html(cityVal + induVal + "展会");
        }
    }
    // 判断选中元素是否具有undefind end
    // 关闭筛选 begin
    function sereenUp(e){
        $(".list_sereenNav").find("li").removeClass("snon");
        $(this).parents(".list_sereen_box").stop(true,true).slideUp("fast");
    }
    // 关闭筛选 end
    // 筛选 end
})