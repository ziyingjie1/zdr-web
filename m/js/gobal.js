// 移动端字体 begin
(function(doc, win) {
    var docEl = doc.documentElement,
        resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
        recalc = function() {
            var clientWidth = docEl.clientWidth;
            if (!clientWidth) return;
            docEl.style.fontSize = 100 * (clientWidth / 750) + 'px';
        };
    if (!doc.addEventListener) return;
    win.addEventListener(resizeEvt, recalc, false);
    doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);
// 移动端字体 end
$(function(){
    // 导航栏 begin
    var navOpen = true;
    $(".index_nav").on("click",function(e){
        if(open){
            $(".index_nav_box").stop(true,true).slideDown('fast').show();
            $(".float-background").stop(true,true).css({
                display: "block",
                visibility: "visible"
            });
            $(".float-background").stop(true,true).animate({"opacity": "1"},500)
            open = false;
        }else{
            $(".index_nav_box").stop(true,true).slideUp('fast').show();
            $(".float-background").stop(true,true).css({
                display: "none",
                visibility: "hidden"
            });
            $(".float-background").stop(true,true).animate({"opacity": "0"},500)
            open = true;
        }
        $(document).one("click", function(){
            $(".index_nav_box").stop(true,true).slideUp('fast');
            $(".float-background").stop(true,true).css({
                display: "none",
                visibility: "hidden"
            });
            $(".float-background").stop(true,true).animate({"opacity": "0"},500)
            open = true;
        });
        $(".float-background").on("click",function(){})
        e.stopPropagation();

         $(".a").scroll(function(event){
            alert(1)
        });
    })
    // 导航栏 end
})
// 监听滚动事件
$(document).ready(function(){
    $(window).scroll(function(){
        if($(".float-background").css('display') == 'block'){
            $(".index_nav_box").stop(true,true).slideUp('fast');
            $(".float-background").stop(true,true).css({
                display: "none",
                visibility: "hidden"
            });
            $(".float-background").stop(true,true).animate({"opacity": "0"},500)
            open = true;
        }
    });
})
// 监听滚动事件