//====================================================
//     init and setcallback
//
//====================================================
// 验证码倒计时
var g_clickenable = 1; //全局变量，定义是否可以进行验证码操作
var g_timechecknum = 60;
function TimerCheckMobileMsgMode(_this)
{
	if(g_clickenable == 1)
	{
		// to do 进行短信发送操作
		g_timechecknum = 60;
		g_clickenable = 0;
		//初始化界面
		$(_this).addClass("f-disable");
		$(_this).html('60秒后重新获取');
	}
	else
	{
		if(g_timechecknum == 0)
		{
			$(_this).html("发送验证码到手机");
			$(_this).removeClass("f-disable");
			g_clickenable = 1;
			return;
		}
		else
		{
			g_timechecknum--;
			$(_this).html(g_timechecknum+'秒后重新获取');
		}
	}
	
	setTimeout(function(){TimerCheckMobileMsgMode(_this)}, 1000);
}

$(document).ready(function($) {

	// 验证码倒计时	
	if($(".f-huoquyanzhengma").size()>0)
	{
		$(".f-huoquyanzhengma").bind("click", function(){
			if(g_clickenable == 1)
			{
				TimerCheckMobileMsgMode(this);
			}
		})
	}
    if($(".m-productdetails").size()>0)
    {
        $(".m-productdetails .onetitle").click(function(){
            $(".m-productdetails .onetitle").removeClass("targeted");
            $(this).addClass("targeted");
            var tmp = $(".m-productdetails .onetitle").index(this);

            $(".m-productdetails .contents .onecontent").removeClass("targeted");
            $(".m-productdetails .contents .onecontent").eq(tmp).addClass("targeted");
        });
    }
		if($(".m-txts").size()>0)
    {
        $(".m-txts .jieshao .more").click(function(){
			$(this).parent().addClass("ons");
        });
    }
	
	if($(".m-txts .wj").size()>0)
    {
		var swiper = new Swiper('.swiper-container-b', {
		  pagination: {
			el: '.swiper-pagination-b',
			type: 'fraction',
		  },		  
          loop: true,
		});
    }
	
	if($(".g-top").size()>0)
    {
	    var swipera = new Swiper('.swiper-container-a', {
		  speed:1000,
		  autoplay: {
			delay: 5000,
			disableOnInteraction: false,
		  },
		  loop: true,
		  pagination: {
			el: '.swiper-pagination-a',
            clickable: true,
		  },
		});
    }
	if($(".m-czuo").size()>0)
    {		
       $(".m-czuo .u-czuo1 .xian").mouseover(function(){
			$(".m-czuo .u-czuo1 .yin").stop(true).animate({"zIndex":"-1","opacity":"0"}, 100);
			$(this).parent().find(".yin").stop(true).animate({"zIndex":"2","opacity":"1"}, 100);
        });
		
		$(document).click(function(event){
			var _con = $('.u-czuo1');   // 设置目标区域
			if(!_con.is(event.target) && _con.has(event.target).length === 0){ // Mark 1
                $(".m-czuo .u-czuo1 .yin").animate({"opacity":"0","zIndex":"-1"}, 100);
			}
		});
    }
	
    if($(".m-suopiao").size()>0)
    {
		$(".m-suopiao .u-renshu .addbtm").click(function(){
			var ppfarther = $(this).parent();
			var tmpval = ppfarther.find("input").val();
			if(tmpval<=9)
			{
				tmpval++;
				var tmphtml = '<div class="u-suopiao">'+
								'<div class="sptit">观众 '+tmpval+' 信息：</div>'+
								'<div class="sptxt">'+
									'<div class="u-input">'+
										'<div class="itit"><span>*</span>姓名：<div class="clear"></div></div>'+
										'<div class="itxt"><input name="name" class="in1" type="text" value=""></div>'+
									'</div>'+
									'<div class="u-input">'+
										'<div class="itit"><span>*</span>手机：</div>'+
										'<div class="itxt"><input name="phone" class="in2" maxlength="11"   type="text" value=""></div>'+
									'</div>'+
									'<div class="u-input">'+
										'<div class="itit"><span>*</span>邮箱：<div class="clear"></div></div>'+
										'<div class="itxt"><input name="email" class="in3" type="text" value=""></div>'+
									'</div>'+
									'<div class="u-input">'+
										'<div class="itit"><span>*</span>公司：<div class="clear"></div></div>'+
										'<div class="itxt"><input class="in4" type="text" value=""></div>'+
									'</div>'+
									'<div class="u-input">'+
										'<div class="itit"><span></span>职位：<div class="clear"></div></div>'+
										'<div class="itxt"><input type="text" value=""></div>'+
									'</div>'+
								'</div>'+
							'</div>';
				var tmp2 = $(".m-suopiao .contents").html();
				tmp2 += tmphtml;
				$(".m-suopiao .contents").html(tmp2);
				
			}else 
			{
			    alert("最多只允许10人！"); 
			}
			ppfarther.find("input").val(tmpval);
			
			
		    //为表单元素添加失去焦点事件
			$("form :input").blur(function(){
				$("form :input").removeClass("on");
				$(this).addClass("on");
				var $parent = $(this).parent();
				$parent.find(".msg").remove(); //删除以前的提醒元素（find()：查找匹配元素集中元素的所有匹配元素）

				//验证姓名
				if($(this).is(".in1")){		 
					var phoneVal = $.trim(this.value);
					if(phoneVal== ""){
						var errorMsg = "姓名不能为空";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
					else{
					}
				}
				//验证手机号码
				if($(this).is(".in2")){
					var phoneVal = $.trim(this.value);
					var regphone = /^1\d{10}$/;
					if(phoneVal== "" || (phoneVal != "" && !regphone.test(phoneVal))){
						var errorMsg = " 请输入正确的手机号码！";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
					else{
					}
				}
				//验证邮箱
				if($(this).is(".in3")){
					var phoneVal = $.trim(this.value);
					var regphone = /.+@.+\.[a-zA-Z]{2,4}$/;
					if(phoneVal== "" || (phoneVal != "" && !regphone.test(phoneVal))){
						var errorMsg = " 请输入正确的邮箱号码！";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
					else{
					}
				}
				//验证公司
				if($(this).is(".in4")){		 
					var phoneVal = $.trim(this.value);
					if(phoneVal== ""){
						var errorMsg = "公司不能为空";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
					else{
					}
				}
			}).keyup(function(){
				//triggerHandler 防止事件执行完后，浏览器自动为标签获得焦点
				$(this).triggerHandler("blur"); 
			}).focus(function(){
				$(this).triggerHandler("blur");
			});
						
			$("input[name='phone']").blur(function () {
				var i = $(this).val();
				var $parent = $(this).parent();
				$("input[name='phone']").not($(this)).each(function () {
					if ($(this).val() == i && $(this).val() != "") {
				        $parent.find(".msg").remove(); 
						var errorMsg = "手机号码不能重复";
						$(".sptxt .u-input:eq(1) .itxt .msg").remove();
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
				});
			 });
			$("input[name='email']").blur(function () {
				var i = $(this).val();
				var $parent = $(this).parent();
				$("input[name='email']").not($(this)).each(function () {
					if ($(this).val() == i && $(this).val() != "") {
				        $parent.find(".msg").remove(); 
						var errorMsg = "邮箱不能重复";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
				});
			});
		});
		$(".m-suopiao .u-renshu .rembtm").click(function(){
			var ppfarther = $(this).parent();
			var tmpval = ppfarther.find("input").val();
			if(tmpval > 1)
			{
				tmpval--;	
			    $(".m-suopiao .u-suopiao:last").remove();
			}else 
			if(tmpval== 1)
			{
			    alert("最少要有1人！");
			}
			ppfarther.find("input").val(tmpval);		
		});
		
		$(".m-suopiao .suo1 .u-xiayibu").click(function(){
			$(this).parent().css("display","none");
			$(this).parent().parent().find(".suo2").css("display","block");
			$(".u-buzhou .zhou").addClass("ons");
			$(".u-buzhou .bu").addClass("ons");
			$(".u-buzhou .bu:last").removeClass("ons");
		});
		
		$(".m-suopiao .suo2 .u-tijiao p").click(function(){
			$(this).parent().parent().css("display","none");
			$(this).parent().parent().parent().find(".suo1").css("display","block");
			$(".u-buzhou .zhou:last").removeClass("ons");
			$(".u-buzhou .bu").removeClass("ons");
			$(".u-buzhou .bu:first").addClass("ons");
		});
    }
	
	//为表单元素添加失去焦点事件
	$("form :input").blur(function(){
		$("form :input").removeClass("on");
		$(this).addClass("on");
		var $parent = $(this).parent();
		$parent.find(".msg").remove(); //删除以前的提醒元素（find()：查找匹配元素集中元素的所有匹配元素）

		//验证姓名
		if($(this).is(".in1")){		 
			var phoneVal = $.trim(this.value);
			if(phoneVal== ""){
				var errorMsg = "姓名不能为空";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else{
			}
		}
		//验证手机号码
		if($(this).is(".in2")){
			var phoneVal = $.trim(this.value);
			var regphone = /^1\d{10}$/;
			if(phoneVal== "" || (phoneVal != "" && !regphone.test(phoneVal))){
				var errorMsg = " 请输入正确的手机号码！";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else{
			}
		}
		//验证邮箱
		if($(this).is(".in3")){
			var phoneVal = $.trim(this.value);
			var regphone = /.+@.+\.[a-zA-Z]{2,4}$/;
			if(phoneVal== "" || (phoneVal != "" && !regphone.test(phoneVal))){
				var errorMsg = " 请输入正确的邮箱号码！";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else{
			}
		}
		//验证公司
		if($(this).is(".in4")){		 
			var phoneVal = $.trim(this.value);
			if(phoneVal== ""){
				var errorMsg = "公司不能为空";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else{
			}
		}
		
		//验证密码
		if($(this).is(".in5")){		 
			var phoneVal = $.trim(this.value);
			if(phoneVal== ""){
				var errorMsg = " 密码不能为空";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else{
			}
		}            
		//验证验证码
		if($(this).is(".in6")){
			var phoneVal = $.trim(this.value);
			var regphone = 333;
			if(phoneVal != regphone){
				var errorMsg = "验证码输入错误！";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
		}
		//验证展位面积
		if($(this).is(".in7")){		 
			var phoneVal = $.trim(this.value);
			if(phoneVal== ""){
				var errorMsg = "展位面积不能为空";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else{
			}
		}
	}).keyup(function(){
		//triggerHandler 防止事件执行完后，浏览器自动为标签获得焦点
		$(this).triggerHandler("blur"); 
	}).focus(function(){
		$(this).triggerHandler("blur");
	});
	
	$("#myform1").click(function(){ 
	
        var mudi=$('input[type="checkbox"][name="mudi"]:checked').size();
        var xingqu=$('input[type="checkbox"][name="xingqu"]:checked').size(); 
		if(mudi==0) 
		{ 
			alert("您参观此展会的主要目的最少选择一个！"); 
			$("input[name='mudi']").get(0).focus(); 
			return false; 
		} else
		if(xingqu==0) 
		{ 
			alert("您所感兴趣的产品最少选择一个！"); 
			$("input[name='xingqu']").get(0).focus(); 
			return false; 
		} 
	}); 
 
});
$(function(){
    // Banner begin
    // banner
    var swiper = new Swiper('.index_banner.swiper-container', {
        pagination: '.index_banner_pagination.swiper-pagination',
        paginationClickable: true,
        autoplay: false,
        autoplayDisableOnInteraction: false,
        loop: true
    });
    // banner end
    // 热门行业 begin
    function changeTab(){
        $(".index_strategy_nav ul").find("li").eq(0).addClass("snon");
        $(".index_city_nav ul").find("li").eq(0).addClass("snon");
        $(".index_news_nav ul").find("li").eq(0).addClass("snon");
        var strSwiper = new Swiper('.index_strategy_sw',{
            speed:500,
            autoHeight: true,
            onSlideChangeStart: function(e){
                $(".index_strategy_nav .snon").removeClass('snon')
                $(".index_strategy_nav li").eq(e.activeIndex).addClass('snon')  
            }
        })
        $(".index_strategy_nav li").on('click',function(e){
            e.preventDefault()
            $(".index_strategy_nav .snon").removeClass('snon')
            $(this).addClass('snon')
            strSwiper.slideTo($(this).index())
        })
        var strSwiper1 = new Swiper('.index_city_sw',{
            speed:500,
            autoHeight: true,
            onSlideChangeStart: function(e){
                $(".index_city_nav .snon").removeClass('snon')
                $(".index_city_nav li").eq(e.activeIndex).addClass('snon')  
            }
        })
        $(".index_city_nav li").on('click',function(e){
            e.preventDefault()
            $(".index_city_nav .snon").removeClass('snon')
            $(this).addClass('snon')
            strSwiper1.slideTo( $(this).index() )
        })
        var strSwiper2 = new Swiper('.index_news_sw',{
            speed:500,
            autoHeight: true,
            onSlideChangeStart: function(e){
                $(".index_news_nav .snon").removeClass('snon')
                $(".index_news_nav li").eq(e.activeIndex).addClass('snon')  
            }
        })
        $(".index_news_nav li").on('click',function(e){
            e.preventDefault()
            $(".index_news_nav .snon").removeClass('snon')
            $(this).addClass('snon')
            strSwiper2.slideTo( $(this).index() )
        })
    }
    setTimeout(changeTab,100);
    // 热门行业 end
})