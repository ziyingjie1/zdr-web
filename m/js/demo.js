// 移动端字体 begin
(function(doc, win) {
    var docEl = doc.documentElement,
        resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
        recalc = function() {
            var clientWidth = docEl.clientWidth;
            if (!clientWidth) return;
            docEl.style.fontSize = 100 * (clientWidth / 750) + 'px';
        };
    if (!doc.addEventListener) return;
    win.addEventListener(resizeEvt, recalc, false);
    doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);
// 移动端字体 end
$(function(){
    // 导航栏 begin
    var navOpen = true;
    $(".index_nav").on("click",function(e){
        if(open){
            $(".index_nav_box").stop(true,true).slideDown('fast').show();
            $(".float-background").stop(true,true).css({
                display: "block",
                visibility: "visible"
            });
            $(".float-background").stop(true,true).animate({"opacity": "1"},500)
            open = false;
        }else{
            $(".index_nav_box").stop(true,true).slideUp('fast').show();
            $(".float-background").stop(true,true).css({
                display: "none",
                visibility: "hidden"
            });
            $(".float-background").stop(true,true).animate({"opacity": "0"},500)
            open = true;
        }
        $(document).one("click", function(){
            $(".index_nav_box").stop(true,true).slideUp('fast');
            $(".float-background").stop(true,true).css({
                display: "none",
                visibility: "hidden"
            });
            $(".float-background").stop(true,true).animate({"opacity": "0"},500)
            open = true;
        });
        e.stopPropagation();
    })
    // 导航栏 end
    // Banner begin
    // banner
    var swiper = new Swiper('.index_banner.swiper-container', {
        pagination: '.index_banner_pagination.swiper-pagination',
        paginationClickable: true,
        autoplay: 4500,
        autoplayDisableOnInteraction: false,
        loop: true
    });
    // banner end
    // 热门行业 begin
    function changeTab(){
        $(".index_strategy_nav ul").find("li").eq(0).addClass("snon");
        $(".index_city_nav ul").find("li").eq(0).addClass("snon");
        $(".index_news_nav ul").find("li").eq(0).addClass("snon");
        var strSwiper = new Swiper('.index_strategy_sw',{
            speed:500,
            autoHeight: true,
            onSlideChangeStart: function(e){
                $(".index_strategy_nav .snon").removeClass('snon')
                $(".index_strategy_nav li").eq(e.activeIndex).addClass('snon')  
            }
        })
        $(".index_strategy_nav li").on('click',function(e){
            e.preventDefault()
            $(".index_strategy_nav .snon").removeClass('snon')
            $(this).addClass('snon')
            strSwiper.slideTo( $(this).index() )
        })
        var strSwiper = new Swiper('.index_city_sw',{
            speed:500,
            autoHeight: true,
            onSlideChangeStart: function(e){
                $(".index_city_nav .snon").removeClass('snon')
                $(".index_city_nav li").eq(e.activeIndex).addClass('snon')  
            }
        })
        $(".index_city_nav li").on('click',function(e){
            e.preventDefault()
            $(".index_city_nav .snon").removeClass('snon')
            $(this).addClass('snon')
            strSwiper.slideTo( $(this).index() )
        })
        var strSwiper = new Swiper('.index_news_sw',{
            speed:500,
            autoHeight: true,
            onSlideChangeStart: function(e){
                $(".index_news_nav .snon").removeClass('snon')
                $(".index_news_nav li").eq(e.activeIndex).addClass('snon')  
            }
        })
        $(".index_news_nav li").on('click',function(e){
            e.preventDefault()
            $(".index_news_nav .snon").removeClass('snon')
            $(this).addClass('snon')
            strSwiper.slideTo( $(this).index() )
        })
    }
    setTimeout(changeTab,100);
    // 热门行业 end
})