//====================================================
//     init and setcallback
//
//====================================================

$(document).ready(function($) {

	if($(".m-txts").size()>0)
    {
        $(".m-txts .jieshao .more").click(function(){
			$(this).parent().addClass("ons");
        });
    }
	
	if($(".m-txts .wj").size()>0)
    {
		var swiper = new Swiper('.swiper-container-b', {
		  pagination: {
			el: '.swiper-pagination-b',
			type: 'fraction',
		  },		  
          loop: true,
		});
    }
	
	if($(".g-top").size()>0)
    {
	    var swipera = new Swiper('.swiper-container-a', {
		  speed:1000,
		  autoplay: {
			delay: 5000,
			disableOnInteraction: false,
		  },
		  loop: true,
		  pagination: {
			el: '.swiper-pagination-a',
            clickable: true,
		  },
		});
    }
	
	if($(".u-wangjie").size()>0)
    {
	    jQuery(".picScroll-left").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"left",autoPlay:true,interTime:5000});
    }
	
	$(window).scroll(function() {
		
		var height1 = $('.g-banner .beijing img').height()-150;
		var d = $(document).scrollTop();
		if( d > height1 )
		{
			$(".g-difu").fadeIn('slow');
		}else 
		{
			
			$(".g-difu").fadeOut('slow');
		}
	});
	
	$('#btnShow').click(function (event) { 	
		event.stopPropagation();  
		$('#divTop').fadeIn('slow');
		return false;
	});
	$('.share-close').click(function (event) { 	
		event.stopPropagation();  
		$('#divTop').fadeOut('slow');
		return false;
	});
	
});
