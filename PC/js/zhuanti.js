
$(document).ready(function ($) {
	if ($(".u-wangjie").size() > 0) {
		jQuery(".picScroll-left").slide({ titCell: ".hd ul", mainCell: ".bd ul", autoPage: true, effect: "left", autoPlay: true, interTime: 5000 });
	}
	$(window).scroll(function () {
		var height1 = $('.g-banner .beijing img').height() - 150;
		var d = $(document).scrollTop();
		if (d > height1) {
			$(".g-difu").fadeIn('slow');
		} else {

			$(".g-difu").fadeOut('slow');
		}
	});

	$('#btnShow').click(function (event) {
		event.stopPropagation();
		$('#divTop').fadeIn('slow');
		return false;
	});
	$('.share-close').click(function (event) {
		event.stopPropagation();
		$('#divTop').fadeOut('slow');
		return false;
	});

});
