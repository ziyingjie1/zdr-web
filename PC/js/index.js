// 验证码倒计时
var g_clickenable = 1; //是否可以进行验证码操作
var g_timechecknum = 60;
function TimerCheckMobileMsgMode(_this) {
	if (g_clickenable == 1) {
		// to do 进行短信发送操作
		g_timechecknum = 60;
		g_clickenable = 0;
		//初始化界面
		$(_this).addClass("f-disable");
		$(_this).html('60秒后重新获取');
	}
	else {
		if (g_timechecknum == 0) {
			$(_this).html("发送验证码到手机");
			$(_this).removeClass("f-disable");
			g_clickenable = 1;
			return;
		}
		else {
			g_timechecknum--;
			$(_this).html(g_timechecknum + '秒后重新获取');
		}
	}
	setTimeout(function () { TimerCheckMobileMsgMode(_this) }, 1000);
}

$(document).ready(function ($) {
	// 验证码倒计时	
	if ($(".f-huoquyanzhengma").size() > 0) {
		$(".f-huoquyanzhengma").bind("click", function () {
			if (g_clickenable == 1) {
				TimerCheckMobileMsgMode(this);
			}
		})
	}

	if ($(".u-czuo").size() > 0) {
		$(".u-czuo .xian1").bind("click", function () {
			var _this = this;
			var tmpdatastatus = $(this).attr("data");
			if (tmpdatastatus == "0") {
				$(this).parent().addClass("ons");
				$(this).attr("data", 1);
			}
			else {
				$(this).parent().removeClass("ons");
				$(this).attr("data", 0);
			}

		})
	}

	if ($(".m-dizhi").size() > 0) {
		$(".m-dizhi .di3").bind("click", function () {
			var _this = this;
			var tmpdatastatus = $(this).attr("data");
			if (tmpdatastatus == "0") {
				$(this).parent().addClass("ons");
				$(this).html("收起");
				$(this).attr("data", 1);
			}
			else {
				$(this).parent().removeClass("ons");
				$(this).html("展开");
				$(this).attr("data", 0);
			}

		})
	}
	if ($(".m-suopiao").size() > 0) {
		$(".m-suopiao .u-renshu .addbtm").click(function () {
			var ppfarther = $(this).parent();
			var tmpval = ppfarther.find("input").val();
			if (tmpval <= 9) {
				tmpval++;
				var tmphtml = '<div class="u-suopiao">' +
					'<div class="sptit">观众 ' + tmpval + ' 信息：</div>' +
					'<div class="sptxt">' +
					'<div class="u-input">' +
					'<div class="itit"><span>*</span>姓名：<div class="clear"></div></div>' +
					'<div class="itxt"><input name="name" class="in1" type="text" value=""></div>' +
					'</div>' +
					'<div class="u-input">' +
					'<div class="itit"><span>*</span>手机：</div>' +
					'<div class="itxt"><input name="phone" class="in2" maxlength="11"   type="text" value=""></div>' +
					'</div>' +
					'<div class="u-input">' +
					'<div class="itit"><span>*</span>邮箱：<div class="clear"></div></div>' +
					'<div class="itxt"><input name="email" class="in3" type="text" value=""></div>' +
					'</div>' +
					'<div class="u-input">' +
					'<div class="itit"><span>*</span>公司：<div class="clear"></div></div>' +
					'<div class="itxt"><input class="in4" type="text" value=""></div>' +
					'</div>' +
					'<div class="u-input">' +
					'<div class="itit"><span></span>职位：<div class="clear"></div></div>' +
					'<div class="itxt"><input type="text" value=""></div>' +
					'</div>' +
					'</div>' +
					'</div>';
				var tmp2 = $(".m-suopiao .contents").html();
				tmp2 += tmphtml;
				$(".m-suopiao .contents").html(tmp2);

			} else {
				alert("最多只允许10人！");
			}
			ppfarther.find("input").val(tmpval);


			//为表单元素添加失去焦点事件
			$("form :input").blur(function () {
				$("form :input").removeClass("on");
				$(this).addClass("on");
				var $parent = $(this).parent();
				$parent.find(".msg").remove(); //删除以前的提醒元素（find()：查找匹配元素集中元素的所有匹配元素）

				//验证姓名
				if ($(this).is(".in1")) {
					var phoneVal = $.trim(this.value);
					if (phoneVal == "") {
						var errorMsg = "姓名不能为空";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
					else {
					}
				}
				//验证手机号码
				if ($(this).is(".in2")) {
					var phoneVal = $.trim(this.value);
					var regphone = /^1\d{10}$/;
					if (phoneVal == "" || (phoneVal != "" && !regphone.test(phoneVal))) {
						var errorMsg = " 请输入正确的手机号码！";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
					else {
					}
				}
				//验证邮箱
				if ($(this).is(".in3")) {
					var phoneVal = $.trim(this.value);
					var regphone = /.+@.+\.[a-zA-Z]{2,4}$/;
					if (phoneVal == "" || (phoneVal != "" && !regphone.test(phoneVal))) {
						var errorMsg = " 请输入正确的邮箱号码！";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
					else {
					}
				}
				//验证公司
				if ($(this).is(".in4")) {
					var phoneVal = $.trim(this.value);
					if (phoneVal == "") {
						var errorMsg = "公司不能为空";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
					else {
					}
				}

			}).keyup(function () {
				//triggerHandler 防止事件执行完后，浏览器自动为标签获得焦点
				$(this).triggerHandler("blur");
			}).focus(function () {
				$(this).triggerHandler("blur");
			});

			$("input[name='phone']").blur(function () {
				var i = $(this).val();
				var $parent = $(this).parent();
				$("input[name='phone']").not($(this)).each(function () {
					if ($(this).val() == i && $(this).val() != "") {
						$parent.find(".msg").remove();
						var errorMsg = "手机号码不能重复";
						$(".sptxt .u-input:eq(1) .itxt .msg").remove();
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
				});
			});
			$("input[name='email']").blur(function () {
				var i = $(this).val();
				var $parent = $(this).parent();
				$("input[name='email']").not($(this)).each(function () {
					if ($(this).val() == i && $(this).val() != "") {
						$parent.find(".msg").remove();
						var errorMsg = "邮箱不能重复";
						$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
					}
				});
			});

		});
		$(".m-suopiao .u-renshu .rembtm").click(function () {
			var ppfarther = $(this).parent();
			var tmpval = ppfarther.find("input").val();
			if (tmpval > 1) {
				tmpval--;
				$(".m-suopiao .u-suopiao:last").remove();
			} else
				if (tmpval == 1) {
					alert("最少要有1人！");
				}
			ppfarther.find("input").val(tmpval);
		});

		$(".m-suopiao .suo1 .u-xiayibu").click(function () {
			$(this).parent().css("display", "none");
			$(this).parent().parent().find(".suo2").css("display", "block");
			$(".u-buzhou .zhou").addClass("ons");
			$(".u-buzhou .bu").addClass("ons");
			$(".u-buzhou .bu:last").removeClass("ons");
		});

		$(".m-suopiao .suo2 .u-tijiao p").click(function () {
			$(this).parent().parent().css("display", "none");
			$(this).parent().parent().parent().find(".suo1").css("display", "block");
			$(".u-buzhou .zhou:last").removeClass("ons");
			$(".u-buzhou .bu").removeClass("ons");
			$(".u-buzhou .bu:first").addClass("ons");
		});
	}
	//为表单元素添加失去焦点事件
	$("form :input").blur(function () {
		$("form :input").removeClass("on");
		$(this).addClass("on");
		var $parent = $(this).parent();
		$parent.find(".msg").remove(); //删除以前的提醒元素（find()：查找匹配元素集中元素的所有匹配元素）

		//验证姓名
		if ($(this).is(".in1")) {
			var phoneVal = $.trim(this.value);
			if (phoneVal == "") {
				var errorMsg = "姓名不能为空";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else {
			}
		}
		//验证手机号码
		if ($(this).is(".in2")) {
			var phoneVal = $.trim(this.value);
			var regphone = /^1\d{10}$/;
			if (phoneVal == "" || (phoneVal != "" && !regphone.test(phoneVal))) {
				var errorMsg = " 请输入正确的手机号码！";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else {
			}
		}
		//验证邮箱
		if ($(this).is(".in3")) {
			var phoneVal = $.trim(this.value);
			var regphone = /.+@.+\.[a-zA-Z]{2,4}$/;
			if (phoneVal == "" || (phoneVal != "" && !regphone.test(phoneVal))) {
				var errorMsg = " 请输入正确的邮箱号码！";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else {
			}
		}
		//验证公司
		if ($(this).is(".in4")) {
			var phoneVal = $.trim(this.value);
			if (phoneVal == "") {
				var errorMsg = "公司不能为空";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else {
			}
		}

		//验证密码
		if ($(this).is(".in5")) {
			var phoneVal = $.trim(this.value);
			if (phoneVal == "") {
				var errorMsg = " 密码不能为空";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else {
			}
		}
		//验证验证码
		if ($(this).is(".in6")) {
			var phoneVal = $.trim(this.value);
			var regphone = 333;
			if (phoneVal != regphone) {
				var errorMsg = "验证码输入错误！";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
		}
		//验证展位面积
		if ($(this).is(".in7")) {
			var phoneVal = $.trim(this.value);
			if (phoneVal == "") {
				var errorMsg = "展位面积不能为空";
				$parent.append("<span class='msg txts'>" + errorMsg + "</span>");
			}
			else {
			}
		}
	}).keyup(function () {
		//triggerHandler 防止事件执行完后，浏览器自动为标签获得焦点
		$(this).triggerHandler("blur");
	}).focus(function () {
		$(this).triggerHandler("blur");
	});

	$("#myform1").click(function () {

		var mudi = $('input[type="checkbox"][name="mudi"]:checked').size();
		var xingqu = $('input[type="checkbox"][name="xingqu"]:checked').size();
		if (mudi == 0) {
			alert("您参观此展会的主要目的最少选择一个！");
			$("input[name='mudi']").get(0).focus();
			return false;
		} else
			if (xingqu == 0) {
				alert("您所感兴趣的产品最少选择一个！");
				$("input[name='xingqu']").get(0).focus();
				return false;
			}
	});
	//首页轮播图 
	if ($(".m-zgimg").size() > 0) {
		jQuery(".slideBox").slide({ mainCell: ".bd ul", effect: "leftLoop", autoPlay: true });
	}
	if ($(".m-productdetails").size() > 0) {
		$(".m-productdetails .onetitle").click(function () {
			$(".m-productdetails .onetitle").removeClass("targeted");
			$(this).addClass("targeted");
			var tmp = $(".m-productdetails .onetitle").index(this);

			$(".m-productdetails .contents .onecontent").removeClass("targeted");
			$(".m-productdetails .contents .onecontent").eq(tmp).addClass("targeted");
		});
	}

});
$(function () {
	var i = 1;
	//导航
	$("#menu-ico").mouseenter(function () {
		$(".menu-nav").css("display", "block");
		$("#menu").css("z-index", "999");
	});

	$("#menu").mouseleave(function () {
		$(".menu-nav").css("display", "none");
		$("#menu").css("z-index", "-1");

	});
	$(".menu-nav").mouseleave(function () {
		$(".menu-nav").css("display", "none");
		$("#menu").css("z-index", "-1");

	});
	$(".menu-nav-ul").hover(function () {
		var id = $(this).attr("data-id");
		$("#" + id).css("display", "block");
		$(this).css("background", "#f2dfd0");
		$("#menu").css("z-index", "999");

	}, function () {
		var id = $(this).attr("data-id");
		$("#" + id).css("display", "none");
		$(this).css("background", "none");
		$("#menu").css("z-index", "99");
	})

	$(".menu-context").hover(function () {
		$(this).css("display", "block");
		$(".menu-nav").css("display", "block");
		$("#menu").css("z-index", "999");
		var id = $(this).attr("id");
		$(".menu-nav ul").each(function () {
			var navid = $(this).attr("data-id");
			if (navid == id) {
				$(this).css("background", "#f2dfd0");

			}
			// alert(navid);
		});

	}, function () {
		$(this).css("display", "none");
		$(".menu-nav-ul").css("background", "none");

		// $("#menu").css("z-index","-1");
	});
	//时间
	$(".item span").mouseenter(function () {
		$(".item span").removeClass("item_on");
		$(this).addClass("item_on");
		var id = $(this).attr("data-id");
		$(".js-Datetime").css("display", "none");
		$("#" + id).css("display", "block");
	});
	//行业推荐导航
	$(".recommend-top-nav li").click(function () {
		$(".recommend-top-nav li").removeClass("js-recommend-nav_on");
		$(this).addClass("js-recommend-nav_on");
		var id = $(this).attr("data-id");
		$(".js-recommend-nav").css("display", "none");
		$("#" + id).css("display", "block");
	});
	//城市推荐行
	$(".city-top-nav li").click(function () {
		$(".city-top-nav li").removeClass("js-recommend-nav_on");
		$(this).addClass("js-recommend-nav_on");
		var id = $(this).attr("data-id");
		$(".js-city").css("display", "none");
		$("#" + id).css("display", "block");

	});
	// 新闻筛选
	//侧栏客服
	var arrayOn = ['kefu_on', 'zhuhezuo_on', 'hezuo_on', 'zhiding_on'];
	var array = ['kefu', 'zhuhezuo', 'hezuo', 'zhiding'];
	$(".custom-SideNav-ul a li").hover(function () {
		$(this).css({ "background": "#ed7c21", "transition": "background 0.3s" });
		$(this).empty();
		var id = $(this).attr("data-id");
		$(this).append("<img src='images/" + arrayOn[id] + ".png'>");
		$("#" + array[id]).css("display", "block");

	}, function () {
		$(this).css("background", "#efeff1");
		$(this).empty();
		var id = $(this).attr("data-id");
		$(this).append("<img src='images/" + array[id] + ".png'>");

		$("#" + array[id]).css("display", "none");
	});

	$(".Online-custom a p").hover(function () {
		$(this).css("display", "block");
		var id = $(this).attr("id");
		$(".custom-SideNav-" + id).css("background", "#ed7c21");
		if (id == "kefu") {
			$(".custom-SideNav-kefu").empty();
			$(".custom-SideNav-kefu").append("<img src='images/kefu_on.png'>");
		} else
			if (id == "zhuhezuo") {
				$(".custom-SideNav-zhuhezuo").empty();
				$(".custom-SideNav-zhuhezuo").append("<img src='images/zhuhezuo_on.png'>");
			}
			else
				if (id == "hezuo") {
					$(".custom-SideNav-hezuo").empty();
					$(".custom-SideNav-hezuo").append("<img src='images/hezuo_on.png'>");
				}
	}, function () {
		$(this).css("display", "none");
		var id = $(this).attr("id");

		$(".custom-SideNav-" + id).css("background", "#efeff1");
		if (id == "kefu") {
			$(".custom-SideNav-kefu").empty();
			$(".custom-SideNav-kefu").append("<img src='images/kefu.png'>");
		} else
			if (id == "zhuhezuo") {
				$(".custom-SideNav-zhuhezuo").empty();
				$(".custom-SideNav-zhuhezuo").append("<img src='images/zhuhezuo.png'>");
			}
			else
				if (id == "hezuo") {
					$(".custom-SideNav-hezuo").empty();
					$(".custom-SideNav-hezuo").append("<img src='images/hezuo.png'>");
				}
	})
});
$(function () {
	$('#menu-ico').on('mouseenter mouseleave', function (e) {
		var w = $(this).width();
		var h = $(this).height();
		var x = (e.pageX - this.offsetLeft - (w / 2)) * (w > h ? (h / w) : 1);
		var y = (e.pageY - this.offsetTop - (h / 2)) * (h > w ? (w / h) : 1);
		var xy = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;
		var eventType = e.type;
		var oName = ['上方', '右侧', '下方', '左侧'];
		if (e.type == 'mouseenter') {
			// $(this).text(oName[xy] + '进入');
		} else {
			// $(this).text(oName[xy] + '离开'); 
			if (oName[xy] != '下方') {
				$(".menu-nav").css("display", "none");

				$("#menu").css("z-index", "-1");

			}
		}
	});
	$(".box-title a span").click(function () {
		$(".box-title a span").removeClass("box-nav_on");
		$(this).addClass("box-nav_on");
		var id = $(this).attr("data-id");
		$(".search-form input").css("display", "none");
		$("#" + id).css("display", "block");
	})
});
