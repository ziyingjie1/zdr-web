$(function () {
	function setZhanText() {
		if ($("#js-city-result").text() == "" && $("#js-hangye-result").text() == "") {
			$("#js-zhanhui").text("");
		}
		else {
			$("#js-zhanhui").text("展会");
		}
	}
	$("#js-clear-div").hide();
	$(".bottom-border").css("border-bottom", "1px #fff solid");

	//行业click
	$("#js-screen li").click(function () {
		var dataId = $(this).attr("data-id");
		var name = $(this).text();
		var id = $(this).attr("id");
		$("#js-jiancai").css("display", "none");
		$("#js-dianzi").css("display", "none");
		$("#js-gongye").css("display", "none");
		$("#js-jiancai2").css("display", "none");
		$("#" + dataId).css("display", "block");
		$("#js-screen li a").removeClass("news-screen-top-js")
		$(this).find("a").addClass("news-screen-top-js");

		$(".j1").hide();
		if (id == "js-no-hangye-buxian") {
			//添加标签
			$("#js-news-screen-hover-ul1").append("<li class='j1'><a href='javascript:void(0)'>行业" + name + "<img src='images/xxx.png'></a></li>");
		}
		else {
			//添加标签
			$("#js-news-screen-hover-ul1").append("<li class='j1'><a href='javascript:void(0)'>" + name + "<img src='images/xxx.png'></a></li>");
		}

		$(".js-hangye-1").hide();
		$("#js-jiancai li a ,#js-jiancai2 li a,#js-gongye li a,#js-dianzi li a").removeClass("news-screen-top-js-son");
		$("#js-clear-div").show();
		$(".bottom-border").css("border-bottom", "1px #eeeeee solid");
	});

	// 时间click
	$("#js-time li").click(function () {
		var dataId = $(this).attr("data-id");
		var name = $(this).text();
		var id = $(this).attr("id");
		$("#js-2019").css("display", "none");
		$("#js-2018").css("display", "none");
		$("#js-2017").css("display", "none");
		$("#" + dataId).css("display", "block");
		$("#js-time li a").removeClass("news-screen-top-js")
		$(this).find("a").addClass("news-screen-top-js");

		$(".j21").hide();
		if (id == "js-no-time-buxian") {
			$("#js-news-screen-hover-ul1").append("<li class='j21'><a href='javascript:void(0)'>时间" + name + "<img src='images/xxx.png'></a></li>");
		}
		else {
			$("#js-news-screen-hover-ul1").append("<li class='j21'><a href='javascript:void(0)'>" + name + "<img src='images/xxx.png'></a></li>");
		}

		$(".js-time-1").hide();
		$("#js-2019 li a,#js-2018 li a,#js-2017 li a").removeClass("news-screen-top-js-son");
		$("#js-clear-div").show();
		$(".bottom-border").css("border-bottom", "1px #eeeeee solid");
	});

	// 城市点击事件
	$("#js-city li").click(function () {
		$("#js-city li a").removeClass("news-screen-top-js-son");
		$("#js-city li a").removeClass("news-screen-top-js");
		$(this).find("a").addClass("news-screen-top-js-son");
		var name = $(this).text();
		var id = $(this).attr("id");

		$(".js-city-1").hide();
		if (id == "js-no-city-buxian") {
			$("#js-news-screen-hover-ul1").append("<li class='js-city-1'><a href='javascript:void(0)'>城市" + name + "<img src='images/xxx.png'></a></li>");
		}
		else {
			$("#js-news-screen-hover-ul1").append("<li class='js-city-1'><a href='javascript:void(0)'>" + name + "<img src='images/xxx.png'></a></li>");
		}
		$("#js-city-result").text(name);

		setZhanText();

		$("#js-clear-div").show();
		$(".bottom-border").css("border-bottom", "1px #eeeeee solid");
	});
	//子行业点击事件
	$("#js-jiancai li ,#js-jiancai2 li,#js-gongye li,#js-dianzi li").click(function () {
		$("#js-jiancai li a ,#js-jiancai2 li a,#js-gongye li a,#js-dianzi li a").removeClass("news-screen-top-js-son");
		$(this).find("a").addClass("news-screen-top-js-son");

		var name = $(this).text();
		$(".js-hangye-1").hide();

		$("#js-news-screen-hover-ul1").append("<li class='js-hangye-1'><a href='javascript:void(0)'>" + name + "<img src='images/xxx.png'></a></li>");
		$("#js-hangye-result").text(name);
		setZhanText();
		$("#js-clear-div").show();
		$(".bottom-border").css("border-bottom", "1px #eeeeee solid");

	});
	//月份点击事件
	$("#js-2019 li,#js-2018 li,#js-2017 li").click(function () {
		$("#js-2019 li a,#js-2018 li a,#js-2017 li a").removeClass("news-screen-top-js-son");
		$(this).find("a").addClass("news-screen-top-js-son");

		var name = $(this).text();

		// alert("fda");
		$(".js-time-1").hide();

		$("#js-news-screen-hover-ul1").append("<li class='js-time-1'><a href='javascript:void(0)'>" + name + "<img src='images/xxx.png'></a></li>");

		$("#js-clear-div").show();
		$(".bottom-border").css("border-bottom", "1px #eeeeee solid");


	});
	//更多城市点击事件
	$("#city-more").click(function () {
		// alert($(".news-screen-top-js-son").html());
		var text = $("#city-more a").text();
		if (text == "+更多") {
			$("#js-city").css("height", "auto");
			$("#city-more a").html("-收起");
		}
		if (text == "-收起") {
			$("#js-city").css("height", "30px");
			$("#city-more a").html("+更多");
		}

	})
	$(".js-city-more").click(function () {
		// alert($(".news-screen-top-js-son").html());
		var text = $("#city-more a").text();
		if (text == "+更多") {
			$("#js-city").css("height", "auto");
			$("#city-more a").html("-收起");
		}
		if (text == "-收起") {
			$("#js-city").css("height", "30px");
			$("#city-more a").html("+更多");
		}

	})
	//后台 筛选通过 js 获取class news-screen-top-js-son 就可以获取到删选选择的值

	//清空
	$("#js-clear").click(function () {
		//清空所有
		$("#js-news-screen-hover-ul1").empty();
		//清空行业class
		$("#js-screen li a").removeClass("news-screen-top-js");
		//给行业不限加class
		$("#js-no-hangye-buxian").find("a").addClass("news-screen-top-js");
		//清空时间class
		$("#js-time li a").removeClass("news-screen-top-js");
		//给时间不限加class
		$("#js-no-time-buxian").find("a").addClass("news-screen-top-js");
		//清空月份class
		$("#js-2019 li a,#js-2018 li a,#js-2017 li a").removeClass("news-screen-top-js-son");
		//清空子行业class
		$("#js-jiancai li a ,#js-jiancai2 li a,#js-gongye li a,#js-dianzi li a").removeClass("news-screen-top-js-son");
		//清楚地区class
		$("#js-city li a").removeClass("news-screen-top-js-son");
		$("#js-city li a").removeClass("news-screen-top-js");
		// 给地区不限加class
		$("#js-no-city-buxian").find("a").addClass("news-screen-top-js-son");
		//收起地区
		$(".news-screen-city-more").css("display", "none");
		$("#city-more a").html("+更多");
		//收起子行业
		$("#js-jiancai").css("display", "none");
		$("#js-dianzi").css("display", "none");
		$("#js-gongye").css("display", "none");
		$("#js-jiancai2").css("display", "none");
		//收起月份
		$("#js-2019").css("display", "none");
		$("#js-2018").css("display", "none");
		$("#js-2017").css("display", "none");
		//结果为空
		$("#js-hangye-result").text("");
		$("#js-city-result").text("");
		setZhanText();
		$("#js-clear-div").hide();
		$(".bottom-border").css("border-bottom", "1px #fff solid");

	});
	//点击标签删除事件
	$("#js-news-screen-hover-ul1").on('click', 'li', function () {
		var cclass = $(this).attr("class");
		var number = $("#js-news-screen-hover-ul1 li:visible").size();
		// alert(cclass);
		if (cclass == "j1") {
			//清空行业class
			$("#js-screen li a").removeClass("news-screen-top-js");
			//清空子行业class
			$("#js-jiancai li a ,#js-jiancai2 li a,#js-gongye li a,#js-dianzi li a").removeClass("news-screen-top-js-son");
			//给行业不限加class
			$("#js-no-hangye-buxian").find("a").addClass("news-screen-top-js");
			//收起子行业
			$("#js-jiancai").css("display", "none");
			$("#js-dianzi").css("display", "none");
			$("#js-gongye").css("display", "none");
			$("#js-jiancai2").css("display", "none");
		}
		else if (cclass == "j21") {
			//清空时间class
			$("#js-time li a").removeClass("news-screen-top-js");
			//给时间不限加class
			$("#js-no-time-buxian").find("a").addClass("news-screen-top-js");
			//清空月份class
			$("#js-2019 li a,#js-2018 li a,#js-2017 li a").removeClass("news-screen-top-js-son");
			//收起月份
			$("#js-2019").css("display", "none");
			$("#js-2018").css("display", "none");
			$("#js-2017").css("display", "none");
		}
		else if (cclass == "js-city-1") {
			//清楚地区class
			$("#js-city li a").removeClass("news-screen-top-js-son");
			$("#js-city li a").removeClass("news-screen-top-js");
			// 给地区不限加class
			$("#js-no-city-buxian").find("a").addClass("news-screen-top-js-son");
			// 收起地区
			$(".news-screen-city-more").css("display", "none");
			$("#city-more a").html("+更多");
			$("#js-city-result").text("");
			setZhanText()
		}
		else if (cclass == "js-time-1") {
			//清空月份class
			$("#js-2019 li a,#js-2018 li a,#js-2017 li a").removeClass("news-screen-top-js-son");
		}
		else if (cclass == "js-hangye-1") {
			//清空子行业classa
			$("#js-jiancai li a ,#js-jiancai2 li a,#js-gongye li a,#js-dianzi li a").removeClass("news-screen-top-js-son");
			$("#js-hangye-result").text("");
			setZhanText();
		}
		if (number == 1) {
			$("#js-clear-div").hide();
			$(".bottom-border").css("border-bottom", "1px #fff solid");
		}

		$(this).hide();
	});
	$("#js-no-city-buxian").click(function () {
		$("#js-city-result").text("");
		setZhanText();
	})
	$("#js-no-hangye-buxian").click(function () {
		$(".js-hangye-1").hide();
		$("#js-hangye-result").text("");
		setZhanText();
	});
	$("#js-no-time-buxian").click(function () {
		$(".js-time-1").hide();
		var name = $(this).text();

	});
	setZhanText();
	$(".pagination li a").click(function () {
		$(".pagination li a").removeClass("pagination_li_on");
		$(this).addClass("pagination_li_on");
	})
	$(".box-title a span").click(function () {
		$(".box-title a span").removeClass("box-nav_on");
		$(this).addClass("box-nav_on");
		var id = $(this).attr("data-id");
		$(".search-form input").css("display", "none");
		$("#" + id).css("display", "block");
	})
})
