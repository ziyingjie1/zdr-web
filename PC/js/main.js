/**
 * TODO 本功能需要layer和jquery插件的支持； 本功能为二次开发。
 * @see 源文件地址：http://sc.chinaz.com/jiaobendemo.aspx?downloadid=0201785541739
 */
main();
var layer;
layui.use('layer', function () {
  layer = layui.layer;
});

function main() {
  if (typeof (layer) != "object" || !layer) {
    setTimeout("main()", 10);
    return;
  }
  var myCalendar = new SimpleCalendar('#calendar', {
    width: '100%',
    height: '500px',
    language: 'CH', //语言
    showLunarCalendar: true, //阴历
    showHoliday: false, //休假-暂时禁用
    showFestival: false, //节日
    showLunarFestival: false, //农历节日
    showSolarTerm: false, //节气
    showMark: true, //标记
    realTime: false, //具体时间
    timeRange: {
      startYear: 2015,
      endYear: 2030
    },
    mark: {},
    markColor: ['u-lv', 'u-huang', 'u-hong'],//记事各个颜色
    main: function (year, month) {
      var industry = $("#industryid").find("option:selected").val();
      var index = -1;
      if (layer) index = layer.msg('正在查询数据......', { icon: 16, shade: 0.6 });
      //@获取数据：
      var data = getData(year, month, 0, industry);
      //数据start
      var resultObj = {}, status = ['当天开幕', '进行中', '当天闭幕'];
      var getDayTotal = new Date(year, month, 0).getDate();
      for (var i = 1; i <= getDayTotal; i++) {
        var array = [];
        var date = year + "-" + month + "-" + (i < 10 ? '0' + i : i);
        for (var j = 0; j < data.length; j++) {
          if (data[j].day == i) {
            array.push({
              title: data[j].name,
              status: data[j].num,
              statusText: status[data[j].num],
              id: data[j].id,
              alias: data[j].alias
            });
            resultObj[date] = array;
          }
        }

      }
      //数据end
      if (layer) layer.close(index);
      return resultObj;

    },
    timeupdate: false,//显示当前的时间HH:mm
    theme: {
      changeAble: false,
      weeks: {
        backgroundColor: '#FBEC9C',
        fontColor: '#4A4A4A',
        fontSize: '20px',
      },
      days: {
        backgroundColor: '#ffffff',
        fontColor: '#565555',
        fontSize: '24px'
      },
      todaycolor: 'orange',
      activeSelectColor: 'orange',
    }
  });
}
function selecthy(obj) {
  return obj.value;
}
function getData(year, month, country, industry) {
  var params = {};
  params.year = year;
  params.month = month;
  params.country = country;
  params.industry = industry;
  var datalist = [
    {
      "id": "294",
      "name": "2021第14届中国昆明国际美容化妆品博览会",
      "alias": "2021昆明春季美博会",
      "city_id": "28",
      "thumbnail": "/uploads/20190726/58ff7de56d50f8fee5cadaab2f99def3.jpg",
      "status": "4",
      "day": "1",
      "exhibition_start_time": "1",
      "exhibition_end_time": "3",
      "start_time": "2021-06-1",
      "end_time": "2021-06-3",
      "num": 0
    },
    {
      "id": "515",
      "name": "2021世环会Wie Tec",
      "alias": "2021世环会",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200714154850827.jpg",
      "status": "4",
      "day": "2",
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 0
    },
    {
      "id": "520",
      "name": "世环会【国际环保展】",
      "alias": "2021世环会国际环保展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200725160479133.jpg",
      "status": "4",
      "day": "2",
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 0
    },
    {
      "id": "521",
      "name": "2021第七届上海国际空气新风展",
      "alias": "2021上海新风展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210331143151752.jpg",
      "status": "4",
      "day": "2",
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 0
    },
    {
      "id": "522",
      "name": "2021第十届上海国际泵管阀展览会",
      "alias": "2021上海泵阀展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200731004396740.jpg",
      "status": "4",
      "day": "2",
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 0
    },
    {
      "id": "525",
      "name": "2021上海国际智慧环保及环境监测展览会",
      "alias": "2021上海智慧环保展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200813144083890.jpg",
      "status": "4",
      "day": "2",
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 0
    },
    {
      "id": "526",
      "name": "第六届上海国际建筑水展",
      "alias": "2021上海建筑水展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200813155634810.jpg",
      "status": "4",
      "day": "2",
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 0
    },
    {
      "id": "527",
      "name": "2021上海国际生态舒适系统展览会",
      "alias": "2021世环会生态舒适展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200813171363975.jpg",
      "status": "4",
      "day": "2",
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 0
    },
    {
      "id": "105",
      "name": "第十四届上海国际水处理展览会",
      "alias": "2021上海国际水展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200722122481976.png",
      "status": "4",
      "day": "2",
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 0
    },
    {
      "id": "207",
      "name": "2021上海第十二届紧固件专业展",
      "alias": "2021上海紧固件展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200722122481976.png.jpg",
      "status": "4",
      "day": "2",
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 0
    },
    {
      "id": "368",
      "name": "2021年第十七届天津工博会",
      "alias": "2021天津工博会",
      "city_id": "3",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200322075096667.jpg",
      "status": "4",
      "day": "3",
      "exhibition_start_time": "3",
      "exhibition_end_time": "6",
      "start_time": "2021-06-3",
      "end_time": "2021-06-6",
      "num": 0
    },
    {
      "id": "302",
      "name": "2021年第33届南京国际美容化妆品博览会",
      "alias": "2021南京美博会",
      "city_id": "14",
      "thumbnail": "/uploads/20190729/8983fe592a76d232fda9ca7da41bd1a5.jpg",
      "status": "4",
      "day": "8",
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 0
    },
    {
      "id": "652",
      "name": "2021厦门工业博览会",
      "alias": "2021厦门工博会(台交会)",
      "city_id": "17",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210409170227031.jpg",
      "status": "4",
      "day": "8",
      "exhibition_start_time": "8",
      "exhibition_end_time": "11",
      "start_time": "2021-06-8",
      "end_time": "2021-06-11",
      "num": 0
    },
    {
      "id": "199",
      "name": "2020第12届中国（郑州）国际消防设备技术展览会",
      "alias": "2021郑州消防展",
      "city_id": "6",
      "thumbnail": "/uploads/20190523/5d62c1803b712a54b7f2a6dd3769da56.JPG",
      "status": "4",
      "day": "8",
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 0
    },
    {
      "id": "203",
      "name": "2021第115届中国文化用品商品交易会",
      "alias": "2021上海文化用品展",
      "city_id": "1",
      "thumbnail": "/uploads/20190523/5278ed2da319ca50a2f996734cd12315.jpg",
      "status": "4",
      "day": "8",
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 0
    },
    {
      "id": "468",
      "name": "第二十一届中国国际石油石化技术装备展览会",
      "alias": "2021北京化工展",
      "city_id": "2",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210224101389776.jpg",
      "status": "4",
      "day": "8",
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 0
    },
    {
      "id": "479",
      "name": "第二十四届中国国际食品添加剂和配料展览会",
      "alias": "FIC2020",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200106134485988.jpg",
      "status": "4",
      "day": "8",
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 0
    },
    {
      "id": "644",
      "name": "消费者科技及创新展览会 Consumer Technology & Innovation Show",
      "alias": "2021CTIS消费科技展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210330124155082.jpg",
      "status": "4",
      "day": "9",
      "exhibition_start_time": "9",
      "exhibition_end_time": "11",
      "start_time": "2021-06-9",
      "end_time": "2021-06-11",
      "num": 0
    },
    {
      "id": "93",
      "name": "2021世界交通运输大会（WTC） 暨交通科技博览会",
      "alias": "2021WTC世界交通运输大会",
      "city_id": "24",
      "thumbnail": "/uploads/20190523/44f30f3008726f4d43cc850f5940bfd6.JPG",
      "status": "4",
      "day": "16",
      "exhibition_start_time": "16",
      "exhibition_end_time": "18",
      "start_time": "2021-06-16",
      "end_time": "2021-06-18",
      "num": 0
    },
    {
      "id": "182",
      "name": "2021“一带一路”中国（吉林）安全与 应急产业博览会",
      "alias": "2021长春应急消防展",
      "city_id": "4",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200312115191720.jpg",
      "status": "4",
      "day": "16",
      "exhibition_start_time": "16",
      "exhibition_end_time": "18",
      "start_time": "2021-06-16",
      "end_time": "2021-06-18",
      "num": 0
    },
    {
      "id": "301",
      "name": "第22届中国（山西）国际美容美发美体化妆品用品博览会",
      "alias": "2021山西太原美博会",
      "city_id": "33",
      "thumbnail": "/uploads/20190729/ee317029ee5fab2d6cb5ec4865a7eb62.jpg",
      "status": "4",
      "day": "17",
      "exhibition_start_time": "17",
      "exhibition_end_time": "19",
      "start_time": "2021-06-17",
      "end_time": "2021-06-19",
      "num": 0
    },
    {
      "id": "643",
      "name": "华食展·中国618中国食材订货节",
      "alias": "2021上海618食材订货节",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210328140739756.jpg",
      "status": "4",
      "day": "18",
      "exhibition_start_time": "18",
      "exhibition_end_time": "20",
      "start_time": "2021-06-18",
      "end_time": "2021-06-20",
      "num": 0
    },
    {
      "id": "454",
      "name": "第十三届中国（成都）礼品及家居用品展览会",
      "alias": "2021成都礼品展",
      "city_id": "5",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210508150476572.jpg",
      "status": "4",
      "day": "18",
      "exhibition_start_time": "18",
      "exhibition_end_time": "20",
      "start_time": "2021-06-18",
      "end_time": "2021-06-20",
      "num": 0
    },
    {
      "id": "136",
      "name": "2021第19届上海国际礼品，赠品及家居用品展览会",
      "alias": "2021上海礼品展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210115140311710.jpg",
      "status": "4",
      "day": "22",
      "exhibition_start_time": "22",
      "exhibition_end_time": "24",
      "start_time": "2021-06-22",
      "end_time": "2021-06-24",
      "num": 0
    },
    {
      "id": "175",
      "name": "2021第35届广州陶瓷工业展",
      "alias": "2021第35届广州陶瓷工业展",
      "city_id": "7",
      "thumbnail": "https://www.zhandaren.cn/ugc/20191224113433388.jpg",
      "status": "4",
      "day": "22",
      "exhibition_start_time": "22",
      "exhibition_end_time": "25",
      "start_time": "2021-06-22",
      "end_time": "2021-06-25",
      "num": 0
    },
    {
      "id": "459",
      "name": "第21届中国国际农用化学品及植保展览会",
      "alias": "CAC2021",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20191231155876711.jpg",
      "status": "4",
      "day": "22",
      "exhibition_start_time": "22",
      "exhibition_end_time": "24",
      "start_time": "2021-06-22",
      "end_time": "2021-06-24",
      "num": 0
    },
    {
      "id": "478",
      "name": "CHINA FOOD上海国际餐饮美食加盟展",
      "alias": "2021上海加盟展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200526100742897.png",
      "status": "4",
      "day": "22",
      "exhibition_start_time": "22",
      "exhibition_end_time": "24",
      "start_time": "2021-06-22",
      "end_time": "2021-06-24",
      "num": 0
    },
    {
      "id": "656",
      "name": "上海国际健康世博会",
      "alias": null,
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210520065451906.jpg",
      "status": "4",
      "day": "23",
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 0
    },
    {
      "id": "657",
      "name": "第二十七届上海国际加工包装展览会",
      "alias": "2021上海加工包装展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210520071148343.jpg",
      "status": "4",
      "day": "23",
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 0
    },
    {
      "id": "659",
      "name": "第十一届上海国际健康产业品牌博览会暨养老与康复博览会",
      "alias": "2021上海健康产业品牌展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210525170025548.jpg",
      "status": "4",
      "day": "23",
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 0
    },
    {
      "id": "660",
      "name": "上海国际健康器械及用品展览会",
      "alias": "2021上海健康器械展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210525173665337.jpg",
      "status": "4",
      "day": "23",
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 0
    },
    {
      "id": "126",
      "name": "第21届广州国际营养品•健康食品和有机产品展览会",
      "alias": "第21届广州营养品健康食品展",
      "city_id": "7",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200319191194947.png",
      "status": "4",
      "day": "24",
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 0
    },
    {
      "id": "128",
      "name": "第30届广州国际大健康产业博览会",
      "alias": "2021广州国际大健康展",
      "city_id": "7",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200319190124244.png",
      "status": "4",
      "day": "24",
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 0
    },
    {
      "id": "501",
      "name": "2021 IGO世界粮油展",
      "alias": "2021广州IGO世界粮油展",
      "city_id": "7",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200406114590950.jpg",
      "status": "4",
      "day": "24",
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 0
    },
    {
      "id": "502",
      "name": "2021第10届广州国际高端饮用水产业博览会",
      "alias": "2021广州饮用水产业展",
      "city_id": "7",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200406120879335.jpg",
      "status": "4",
      "day": "24",
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 0
    },
    {
      "id": "388",
      "name": "2021第十一届中国上海国际汽车内饰与外饰展览会",
      "alias": "2021上海汽车内外饰展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20201028101870543.png",
      "status": "4",
      "day": "27",
      "exhibition_start_time": "27",
      "exhibition_end_time": "29",
      "start_time": "2021-06-27",
      "end_time": "2021-06-29",
      "num": 0
    },
    {
      "id": "467",
      "name": "2021第十一届中国北京国际煤化工展览会",
      "alias": "2020北京煤化工展",
      "city_id": "2",
      "thumbnail": "https://www.zhandaren.cn/ugc/20200103120080756.jpg",
      "status": "4",
      "day": "28",
      "exhibition_start_time": "28",
      "exhibition_end_time": "30",
      "start_time": "2021-06-28",
      "end_time": "2021-06-30",
      "num": 0
    },
    {
      "id": "658",
      "name": "2021中国（上海）国际显示技术及应用创新展",
      "alias": "2021 DIC EXPO显示展",
      "city_id": "1",
      "thumbnail": "https://www.zhandaren.cn/ugc/20210524180573199.jpg",
      "status": "4",
      "day": "30",
      "exhibition_start_time": "30",
      "exhibition_end_time": "2",
      "start_time": "2021-06-30",
      "end_time": "2021-07-2",
      "num": 0
    },
    {
      "id": "294",
      "name": "2021第14届中国昆明国际美容化妆品博览会",
      "alias": "2021昆明春季美博会",
      "day": 2,
      "exhibition_start_time": "1",
      "exhibition_end_time": "3",
      "start_time": "2021-06-1",
      "end_time": "2021-06-3",
      "num": 1
    },
    {
      "id": "294",
      "name": "2021第14届中国昆明国际美容化妆品博览会",
      "alias": "2021昆明春季美博会",
      "day": 3,
      "exhibition_start_time": "1",
      "exhibition_end_time": "3",
      "start_time": "2021-06-1",
      "end_time": "2021-06-3",
      "num": 2
    },
    {
      "id": "515",
      "name": "2021世环会Wie Tec",
      "alias": "2021世环会",
      "day": 3,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 1
    },
    {
      "id": "515",
      "name": "2021世环会Wie Tec",
      "alias": "2021世环会",
      "day": 4,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 2
    },
    {
      "id": "520",
      "name": "世环会【国际环保展】",
      "alias": "2021世环会国际环保展",
      "day": 3,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 1
    },
    {
      "id": "520",
      "name": "世环会【国际环保展】",
      "alias": "2021世环会国际环保展",
      "day": 4,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 2
    },
    {
      "id": "521",
      "name": "2021第七届上海国际空气新风展",
      "alias": "2021上海新风展",
      "day": 3,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 1
    },
    {
      "id": "521",
      "name": "2021第七届上海国际空气新风展",
      "alias": "2021上海新风展",
      "day": 4,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 2
    },
    {
      "id": "522",
      "name": "2021第十届上海国际泵管阀展览会",
      "alias": "2021上海泵阀展",
      "day": 3,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 1
    },
    {
      "id": "522",
      "name": "2021第十届上海国际泵管阀展览会",
      "alias": "2021上海泵阀展",
      "day": 4,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 2
    },
    {
      "id": "525",
      "name": "2021上海国际智慧环保及环境监测展览会",
      "alias": "2021上海智慧环保展",
      "day": 3,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 1
    },
    {
      "id": "525",
      "name": "2021上海国际智慧环保及环境监测展览会",
      "alias": "2021上海智慧环保展",
      "day": 4,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 2
    },
    {
      "id": "526",
      "name": "第六届上海国际建筑水展",
      "alias": "2021上海建筑水展",
      "day": 3,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 1
    },
    {
      "id": "526",
      "name": "第六届上海国际建筑水展",
      "alias": "2021上海建筑水展",
      "day": 4,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 2
    },
    {
      "id": "527",
      "name": "2021上海国际生态舒适系统展览会",
      "alias": "2021世环会生态舒适展",
      "day": 3,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 1
    },
    {
      "id": "527",
      "name": "2021上海国际生态舒适系统展览会",
      "alias": "2021世环会生态舒适展",
      "day": 4,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 2
    },
    {
      "id": "105",
      "name": "第十四届上海国际水处理展览会",
      "alias": "2021上海国际水展",
      "day": 3,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 1
    },
    {
      "id": "105",
      "name": "第十四届上海国际水处理展览会",
      "alias": "2021上海国际水展",
      "day": 4,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 2
    },
    {
      "id": "207",
      "name": "2021上海第十二届紧固件专业展",
      "alias": "2021上海紧固件展",
      "day": 3,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 1
    },
    {
      "id": "207",
      "name": "2021上海第十二届紧固件专业展",
      "alias": "2021上海紧固件展",
      "day": 4,
      "exhibition_start_time": "2",
      "exhibition_end_time": "4",
      "start_time": "2021-06-2",
      "end_time": "2021-06-4",
      "num": 2
    },
    {
      "id": "368",
      "name": "2021年第十七届天津工博会",
      "alias": "2021天津工博会",
      "day": 4,
      "exhibition_start_time": "3",
      "exhibition_end_time": "6",
      "start_time": "2021-06-3",
      "end_time": "2021-06-6",
      "num": 1
    },
    {
      "id": "368",
      "name": "2021年第十七届天津工博会",
      "alias": "2021天津工博会",
      "day": 5,
      "exhibition_start_time": "3",
      "exhibition_end_time": "6",
      "start_time": "2021-06-3",
      "end_time": "2021-06-6",
      "num": 1
    },
    {
      "id": "368",
      "name": "2021年第十七届天津工博会",
      "alias": "2021天津工博会",
      "day": 6,
      "exhibition_start_time": "3",
      "exhibition_end_time": "6",
      "start_time": "2021-06-3",
      "end_time": "2021-06-6",
      "num": 2
    },
    {
      "id": "302",
      "name": "2021年第33届南京国际美容化妆品博览会",
      "alias": "2021南京美博会",
      "day": 9,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 1
    },
    {
      "id": "302",
      "name": "2021年第33届南京国际美容化妆品博览会",
      "alias": "2021南京美博会",
      "day": 10,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 2
    },
    {
      "id": "652",
      "name": "2021厦门工业博览会",
      "alias": "2021厦门工博会(台交会)",
      "day": 9,
      "exhibition_start_time": "8",
      "exhibition_end_time": "11",
      "start_time": "2021-06-8",
      "end_time": "2021-06-11",
      "num": 1
    },
    {
      "id": "652",
      "name": "2021厦门工业博览会",
      "alias": "2021厦门工博会(台交会)",
      "day": 10,
      "exhibition_start_time": "8",
      "exhibition_end_time": "11",
      "start_time": "2021-06-8",
      "end_time": "2021-06-11",
      "num": 1
    },
    {
      "id": "652",
      "name": "2021厦门工业博览会",
      "alias": "2021厦门工博会(台交会)",
      "day": 11,
      "exhibition_start_time": "8",
      "exhibition_end_time": "11",
      "start_time": "2021-06-8",
      "end_time": "2021-06-11",
      "num": 2
    },
    {
      "id": "199",
      "name": "2020第12届中国（郑州）国际消防设备技术展览会",
      "alias": "2021郑州消防展",
      "day": 9,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 1
    },
    {
      "id": "199",
      "name": "2020第12届中国（郑州）国际消防设备技术展览会",
      "alias": "2021郑州消防展",
      "day": 10,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 2
    },
    {
      "id": "203",
      "name": "2021第115届中国文化用品商品交易会",
      "alias": "2021上海文化用品展",
      "day": 9,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 1
    },
    {
      "id": "203",
      "name": "2021第115届中国文化用品商品交易会",
      "alias": "2021上海文化用品展",
      "day": 10,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 2
    },
    {
      "id": "468",
      "name": "第二十一届中国国际石油石化技术装备展览会",
      "alias": "2021北京化工展",
      "day": 9,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 1
    },
    {
      "id": "468",
      "name": "第二十一届中国国际石油石化技术装备展览会",
      "alias": "2021北京化工展",
      "day": 10,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 2
    },
    {
      "id": "479",
      "name": "第二十四届中国国际食品添加剂和配料展览会",
      "alias": "FIC2020",
      "day": 9,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 1
    },
    {
      "id": "479",
      "name": "第二十四届中国国际食品添加剂和配料展览会",
      "alias": "FIC2020",
      "day": 10,
      "exhibition_start_time": "8",
      "exhibition_end_time": "10",
      "start_time": "2021-06-8",
      "end_time": "2021-06-10",
      "num": 2
    },
    {
      "id": "644",
      "name": "消费者科技及创新展览会 Consumer Technology & Innovation Show",
      "alias": "2021CTIS消费科技展",
      "day": 10,
      "exhibition_start_time": "9",
      "exhibition_end_time": "11",
      "start_time": "2021-06-9",
      "end_time": "2021-06-11",
      "num": 1
    },
    {
      "id": "644",
      "name": "消费者科技及创新展览会 Consumer Technology & Innovation Show",
      "alias": "2021CTIS消费科技展",
      "day": 11,
      "exhibition_start_time": "9",
      "exhibition_end_time": "11",
      "start_time": "2021-06-9",
      "end_time": "2021-06-11",
      "num": 2
    },
    {
      "id": "93",
      "name": "2021世界交通运输大会（WTC） 暨交通科技博览会",
      "alias": "2021WTC世界交通运输大会",
      "day": 17,
      "exhibition_start_time": "16",
      "exhibition_end_time": "18",
      "start_time": "2021-06-16",
      "end_time": "2021-06-18",
      "num": 1
    },
    {
      "id": "93",
      "name": "2021世界交通运输大会（WTC） 暨交通科技博览会",
      "alias": "2021WTC世界交通运输大会",
      "day": 18,
      "exhibition_start_time": "16",
      "exhibition_end_time": "18",
      "start_time": "2021-06-16",
      "end_time": "2021-06-18",
      "num": 2
    },
    {
      "id": "182",
      "name": "2021“一带一路”中国（吉林）安全与 应急产业博览会",
      "alias": "2021长春应急消防展",
      "day": 17,
      "exhibition_start_time": "16",
      "exhibition_end_time": "18",
      "start_time": "2021-06-16",
      "end_time": "2021-06-18",
      "num": 1
    },
    {
      "id": "182",
      "name": "2021“一带一路”中国（吉林）安全与 应急产业博览会",
      "alias": "2021长春应急消防展",
      "day": 18,
      "exhibition_start_time": "16",
      "exhibition_end_time": "18",
      "start_time": "2021-06-16",
      "end_time": "2021-06-18",
      "num": 2
    },
    {
      "id": "301",
      "name": "第22届中国（山西）国际美容美发美体化妆品用品博览会",
      "alias": "2021山西太原美博会",
      "day": 18,
      "exhibition_start_time": "17",
      "exhibition_end_time": "19",
      "start_time": "2021-06-17",
      "end_time": "2021-06-19",
      "num": 1
    },
    {
      "id": "301",
      "name": "第22届中国（山西）国际美容美发美体化妆品用品博览会",
      "alias": "2021山西太原美博会",
      "day": 19,
      "exhibition_start_time": "17",
      "exhibition_end_time": "19",
      "start_time": "2021-06-17",
      "end_time": "2021-06-19",
      "num": 2
    },
    {
      "id": "643",
      "name": "华食展·中国618中国食材订货节",
      "alias": "2021上海618食材订货节",
      "day": 19,
      "exhibition_start_time": "18",
      "exhibition_end_time": "20",
      "start_time": "2021-06-18",
      "end_time": "2021-06-20",
      "num": 1
    },
    {
      "id": "643",
      "name": "华食展·中国618中国食材订货节",
      "alias": "2021上海618食材订货节",
      "day": 20,
      "exhibition_start_time": "18",
      "exhibition_end_time": "20",
      "start_time": "2021-06-18",
      "end_time": "2021-06-20",
      "num": 2
    },
    {
      "id": "454",
      "name": "第十三届中国（成都）礼品及家居用品展览会",
      "alias": "2021成都礼品展",
      "day": 19,
      "exhibition_start_time": "18",
      "exhibition_end_time": "20",
      "start_time": "2021-06-18",
      "end_time": "2021-06-20",
      "num": 1
    },
    {
      "id": "454",
      "name": "第十三届中国（成都）礼品及家居用品展览会",
      "alias": "2021成都礼品展",
      "day": 20,
      "exhibition_start_time": "18",
      "exhibition_end_time": "20",
      "start_time": "2021-06-18",
      "end_time": "2021-06-20",
      "num": 2
    },
    {
      "id": "136",
      "name": "2021第19届上海国际礼品，赠品及家居用品展览会",
      "alias": "2021上海礼品展",
      "day": 23,
      "exhibition_start_time": "22",
      "exhibition_end_time": "24",
      "start_time": "2021-06-22",
      "end_time": "2021-06-24",
      "num": 1
    },
    {
      "id": "136",
      "name": "2021第19届上海国际礼品，赠品及家居用品展览会",
      "alias": "2021上海礼品展",
      "day": 24,
      "exhibition_start_time": "22",
      "exhibition_end_time": "24",
      "start_time": "2021-06-22",
      "end_time": "2021-06-24",
      "num": 2
    },
    {
      "id": "175",
      "name": "2021第35届广州陶瓷工业展",
      "alias": "2021第35届广州陶瓷工业展",
      "day": 23,
      "exhibition_start_time": "22",
      "exhibition_end_time": "25",
      "start_time": "2021-06-22",
      "end_time": "2021-06-25",
      "num": 1
    },
    {
      "id": "175",
      "name": "2021第35届广州陶瓷工业展",
      "alias": "2021第35届广州陶瓷工业展",
      "day": 24,
      "exhibition_start_time": "22",
      "exhibition_end_time": "25",
      "start_time": "2021-06-22",
      "end_time": "2021-06-25",
      "num": 1
    },
    {
      "id": "175",
      "name": "2021第35届广州陶瓷工业展",
      "alias": "2021第35届广州陶瓷工业展",
      "day": 25,
      "exhibition_start_time": "22",
      "exhibition_end_time": "25",
      "start_time": "2021-06-22",
      "end_time": "2021-06-25",
      "num": 2
    },
    {
      "id": "459",
      "name": "第21届中国国际农用化学品及植保展览会",
      "alias": "CAC2021",
      "day": 23,
      "exhibition_start_time": "22",
      "exhibition_end_time": "24",
      "start_time": "2021-06-22",
      "end_time": "2021-06-24",
      "num": 1
    },
    {
      "id": "459",
      "name": "第21届中国国际农用化学品及植保展览会",
      "alias": "CAC2021",
      "day": 24,
      "exhibition_start_time": "22",
      "exhibition_end_time": "24",
      "start_time": "2021-06-22",
      "end_time": "2021-06-24",
      "num": 2
    },
    {
      "id": "478",
      "name": "CHINA FOOD上海国际餐饮美食加盟展",
      "alias": "2021上海加盟展",
      "day": 23,
      "exhibition_start_time": "22",
      "exhibition_end_time": "24",
      "start_time": "2021-06-22",
      "end_time": "2021-06-24",
      "num": 1
    },
    {
      "id": "478",
      "name": "CHINA FOOD上海国际餐饮美食加盟展",
      "alias": "2021上海加盟展",
      "day": 24,
      "exhibition_start_time": "22",
      "exhibition_end_time": "24",
      "start_time": "2021-06-22",
      "end_time": "2021-06-24",
      "num": 2
    },
    {
      "id": "656",
      "name": "上海国际健康世博会",
      "alias": null,
      "day": 24,
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 1
    },
    {
      "id": "656",
      "name": "上海国际健康世博会",
      "alias": null,
      "day": 25,
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 2
    },
    {
      "id": "657",
      "name": "第二十七届上海国际加工包装展览会",
      "alias": "2021上海加工包装展",
      "day": 24,
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 1
    },
    {
      "id": "657",
      "name": "第二十七届上海国际加工包装展览会",
      "alias": "2021上海加工包装展",
      "day": 25,
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 2
    },
    {
      "id": "659",
      "name": "第十一届上海国际健康产业品牌博览会暨养老与康复博览会",
      "alias": "2021上海健康产业品牌展",
      "day": 24,
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 1
    },
    {
      "id": "659",
      "name": "第十一届上海国际健康产业品牌博览会暨养老与康复博览会",
      "alias": "2021上海健康产业品牌展",
      "day": 25,
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 2
    },
    {
      "id": "660",
      "name": "上海国际健康器械及用品展览会",
      "alias": "2021上海健康器械展",
      "day": 24,
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 1
    },
    {
      "id": "660",
      "name": "上海国际健康器械及用品展览会",
      "alias": "2021上海健康器械展",
      "day": 25,
      "exhibition_start_time": "23",
      "exhibition_end_time": "25",
      "start_time": "2021-06-23",
      "end_time": "2021-06-25",
      "num": 2
    },
    {
      "id": "126",
      "name": "第21届广州国际营养品•健康食品和有机产品展览会",
      "alias": "第21届广州营养品健康食品展",
      "day": 25,
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 1
    },
    {
      "id": "126",
      "name": "第21届广州国际营养品•健康食品和有机产品展览会",
      "alias": "第21届广州营养品健康食品展",
      "day": 26,
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 2
    },
    {
      "id": "128",
      "name": "第30届广州国际大健康产业博览会",
      "alias": "2021广州国际大健康展",
      "day": 25,
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 1
    },
    {
      "id": "128",
      "name": "第30届广州国际大健康产业博览会",
      "alias": "2021广州国际大健康展",
      "day": 26,
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 2
    },
    {
      "id": "501",
      "name": "2021 IGO世界粮油展",
      "alias": "2021广州IGO世界粮油展",
      "day": 25,
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 1
    },
    {
      "id": "501",
      "name": "2021 IGO世界粮油展",
      "alias": "2021广州IGO世界粮油展",
      "day": 26,
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 2
    },
    {
      "id": "502",
      "name": "2021第10届广州国际高端饮用水产业博览会",
      "alias": "2021广州饮用水产业展",
      "day": 25,
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 1
    },
    {
      "id": "502",
      "name": "2021第10届广州国际高端饮用水产业博览会",
      "alias": "2021广州饮用水产业展",
      "day": 26,
      "exhibition_start_time": "24",
      "exhibition_end_time": "26",
      "start_time": "2021-06-24",
      "end_time": "2021-06-26",
      "num": 2
    },
    {
      "id": "388",
      "name": "2021第十一届中国上海国际汽车内饰与外饰展览会",
      "alias": "2021上海汽车内外饰展",
      "day": 28,
      "exhibition_start_time": "27",
      "exhibition_end_time": "29",
      "start_time": "2021-06-27",
      "end_time": "2021-06-29",
      "num": 1
    },
    {
      "id": "388",
      "name": "2021第十一届中国上海国际汽车内饰与外饰展览会",
      "alias": "2021上海汽车内外饰展",
      "day": 29,
      "exhibition_start_time": "27",
      "exhibition_end_time": "29",
      "start_time": "2021-06-27",
      "end_time": "2021-06-29",
      "num": 2
    },
    {
      "id": "467",
      "name": "2021第十一届中国北京国际煤化工展览会",
      "alias": "2020北京煤化工展",
      "day": 29,
      "exhibition_start_time": "28",
      "exhibition_end_time": "30",
      "start_time": "2021-06-28",
      "end_time": "2021-06-30",
      "num": 1
    },
    {
      "id": "467",
      "name": "2021第十一届中国北京国际煤化工展览会",
      "alias": "2020北京煤化工展",
      "day": 30,
      "exhibition_start_time": "28",
      "exhibition_end_time": "30",
      "start_time": "2021-06-28",
      "end_time": "2021-06-30",
      "num": 2
    }
  ];
  // $.ajax({
  //   url: "/getCalendarData",
  //   type: "POST",
  //   data: params,
  //   async: false,
  //   success: function (res) {
  //     datalist = res.payload;
  //   }
  // });
  return datalist;
}

function getIndustryData() {
  var content = "";
  // $.ajax({
  //   url: "/getIndustryData",
  //   dataType: "json",
  //   async: false,
  //   success: function (res) {
  var res = { "code": 1, "msg": "OK", "payload": [{ "id": "175", "name": "\u56ed\u6797" }, { "id": "30", "name": "\u6696\u901a" }, { "id": "191", "name": "\u6c11\u5bbf" }, { "id": "63", "name": "\u6a21\u5177" }, { "id": "165", "name": "\u4e50\u5668" }, { "id": "147", "name": "\u8f6e\u80ce" }, { "id": "3", "name": "\u52b3\u4fdd" }, { "id": "187", "name": "\u5065\u8eab" }, { "id": "92", "name": "\u5bb6\u5177" }, { "id": "142", "name": "\u76ae\u9769" }, { "id": "50", "name": "\u77f3\u6cb9" }, { "id": "77", "name": "\u5851\u80f6" }, { "id": "174", "name": "\u6e14\u4e1a" }, { "id": "159", "name": "\u6e38\u620f" }, { "id": "129", "name": "\u5a74\u7ae5" }, { "id": "173", "name": "\u755c\u7267" }, { "id": "128", "name": "\u73a9\u5177" }, { "id": "76", "name": "\u6d82\u6599" }, { "id": "53", "name": "\u7167\u660e" }, { "id": "85", "name": "\u751f\u7269" }, { "id": "58", "name": "\u673a\u68b0" }, { "id": "32", "name": "\u5efa\u6750" }, { "id": "57", "name": "\u5de5\u4e1a" }, { "id": "132", "name": "\u5305\u88c5" }, { "id": "44", "name": "\u7ba1\u6750" }, { "id": "153", "name": "\u623f\u8f66" }, { "id": "144", "name": "\u670d\u88c5" }, { "id": "135", "name": "\u7eba\u7ec7" }, { "id": "26", "name": "\u8239\u8236" }, { "id": "8", "name": "\u996e\u6599" }, { "id": "23", "name": "\u822a\u7a7a" }, { "id": "1", "name": "\u5b89\u9632" }, { "id": "2", "name": "\u5b89\u5168" }, { "id": "75", "name": "\u73af\u4fdd" }, { "id": "54", "name": "\u7535\u6c60" }, { "id": "121", "name": "\u8fd0\u52a8" }, { "id": "22", "name": "\u4ea4\u901a" }, { "id": "158", "name": "\u901a\u8baf" }, { "id": "25", "name": "\u7269\u6d41" }, { "id": "29", "name": "\u6cf5\u9600" }, { "id": "108", "name": "\u6587\u5316" }, { "id": "15", "name": "\u8336\u53f6" }, { "id": "12", "name": "\u7cd6\u9152" }, { "id": "145", "name": "\u5370\u82b1" }, { "id": "117", "name": "\u6e38\u4e50" }, { "id": "79", "name": "\u533b\u7597" }, { "id": "100", "name": "\u7535\u5546" }, { "id": "73", "name": "\u5316\u5de5" }, { "id": "151", "name": "\u6469\u914d" }, { "id": "91", "name": "\u5bb6\u7eba" }, { "id": "172", "name": "\u519c\u4e1a" }, { "id": "146", "name": "\u6c7d\u8f66" }, { "id": "116", "name": "\u6237\u5916" }, { "id": "154", "name": "\u7535\u5b50" }, { "id": "126", "name": "\u793c\u54c1" }, { "id": "104", "name": "\u6559\u80b2" }, { "id": "84", "name": "\u4fdd\u5065" }, { "id": "5", "name": "\u6d88\u9632" }, { "id": "48", "name": "\u80fd\u6e90" }, { "id": "96", "name": "\u96f6\u552e" }, { "id": "80", "name": "\u7f8e\u5bb9" }, { "id": "65", "name": "\u673a\u5e8a" }, { "id": "107", "name": "\u52a0\u76df" }, { "id": "7", "name": "\u98df\u54c1" }] }
  if (res.code == 1) {
    var html = "";
    $.each(res.payload, function (index, item) {
      html += "<option value='" + res.payload[index].id + "'>" + res.payload[index].name + "</option>";
    })
    content = html;
  }
  //   }
  // });
  return content;
}